﻿module.exports = {
    entry: {
        app: "./Scripts/index.js",
        vendors: ['angular',
                  'angular-cookies',
                  '@uirouter/angularjs',
                  'angular-ui-bootstrap']
    },
    output: {
        path: __dirname + "/public/js",
        filename: "[name].js"
    }
}