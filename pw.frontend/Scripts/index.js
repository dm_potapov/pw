﻿var app = angular.module("pw", ['ui.router', 'ui.bootstrap', 'ngCookies']);

require("./services/authService.js");
require("./services/transactionsService.js");
require("./services/authInterceptorService.js");

require("./components/main.js");
require("./components/login.js");
require("./components/register.js");
require("./components/transactions.js");
require("./components/addTransaction.js");

app.config(function ($stateProvider, $httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptorService');
    $stateProvider
        .state({
            name: 'main',
            url: '/',
            component: 'main'
        })
        .state(
        {
            name: 'login',
            url: '/login',
            component: 'login'
        })
        .state(
        {
            name: 'register',
            url: '/register',
            component: 'register'
        })
        .state(
        {
            name: 'transactions',
            url: '/transactions',
            component: 'transactions'
        })
        .state(
        {
            name: 'transaction_add',
            url: '/transactions/add',
            component: 'addTransaction'
        });
})
.run(['AuthService', function(AuthService) {
    AuthService.fillAuthData();
}])
.controller('headerActionsCtrl', ['AuthService', '$scope', function(AuthService, $scope) {
    $scope.logOut = function () {
        AuthService.logOut();
    }
}]);