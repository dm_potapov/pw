﻿angular.module('pw')
.factory('TransactionsService', ['$http', function ($http) {
    var transactionsServiceFactory = {};

    var serviceBase = 'http://localhost:1337/';

    var _getHistory = function (filter) {
        return $http.post(serviceBase + 'api/transactions/history', filter).then(function (response) {
            return response;
        });
    };

    var _getReceiverSuggestions = function (search) {
        return $http.get(serviceBase + 'api/account/getByFullName?search=' + search)
        .then(function (response) {
            return response;
        });
    };

    var _commitTransaction = function (data) {
        return $http.post(serviceBase + 'api/transactions/commit', data).then(function (response) {
            return response;
        });
    }

    transactionsServiceFactory.getHistory = _getHistory;
    transactionsServiceFactory.commitTransaction = _commitTransaction;
    transactionsServiceFactory.getReceiverSuggestions = _getReceiverSuggestions

    return transactionsServiceFactory;
}]);