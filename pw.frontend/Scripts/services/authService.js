﻿angular.module('pw')
.factory('AuthService', ['$http', '$q', '$cookies', '$rootScope',
    function ($http, $q, $cookies, $rootScope) {

        var serviceBase = 'http://localhost:1337/';
        var authServiceFactory = {};

        var _register = function (registration) {

            _logOut();

            return $http.post(serviceBase + 'api/login/signup', registration).then(function (response) {
                return response;
            });

        };

        var _login = function (loginData) {

            var deferred = $q.defer();

            $http.post(serviceBase + 'api/login/signin', loginData,
                { headers: { 'Content-Type': 'application/json' } }).then(function (response) {

                    $cookies.put('sessionToken', response.data.Token);
                    $cookies.put('accountInfo', JSON.stringify(response.data.AccountInfo));

                    $rootScope.isLoggedIn = true;
                    $rootScope.accountInfo = response.data.AccountInfo;

                    deferred.resolve(response);

                }, function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });

            return deferred.promise;

        };

        var _logOut = function () {
            $cookies.remove('accountInfo');
            $rootScope.isLoggedIn = false;
        };

        var _fillAuthData = function () {
            var accountInfo = $cookies.get('accountInfo');
            if (accountInfo) {
                $rootScope.isLoggedIn = true;
                $rootScope.accountInfo = JSON.parse(accountInfo);
            }
        }

        authServiceFactory.register = _register;
        authServiceFactory.login = _login;
        authServiceFactory.logOut = _logOut;
        authServiceFactory.fillAuthData = _fillAuthData;

        return authServiceFactory;
}]);