﻿angular.module('pw')
.component('login', {
    templateUrl: "templates/login.html",
    resolve: {
        isLoggedIn: "="
    },
    controller: ['$rootScope', 'AuthService', '$state',
        function ($rootScope, AuthService, $state) {
            if ($rootScope.isLoggedIn) {
                $state.go('main');
            }
            $ctrl = this;
            $ctrl.formData = {
                EMail: "",
                Password: ""
            };
            $ctrl.submit = function () {
                AuthService.login($ctrl.formData).then(function (response) {
                    $state.go('main');
                },
                function (err)
                {
                    alert(err.data.Message);
                })
        }
    }]
})