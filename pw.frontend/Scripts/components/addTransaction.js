﻿angular.module('pw')
.component('addTransaction', {
    templateUrl: "templates/transaction_commit.html",
    controller: ['TransactionsService', '$state', '$rootScope',
        function (TransactionsService, $state, $rootScope) {
            var $ctrl = this;
            if (!$rootScope.isLoggedIn) {
                $state.go('login');
            }
            $ctrl.search = "";
            $ctrl.receiverSuggestions = [];
            $ctrl.selectedReceiver = null;
            $ctrl.amount = 0;
            $ctrl.commitTransaction = function () {
                if ($rootScope.accountInfo.Amount < $ctrl.amount) {
                    alert("cannot do transaction because of lack of funds");
                }
                TransactionsService.commitTransaction({
                    ReceiverId: $ctrl.selectedReceiver.Id,
                    Amount: $ctrl.amount
                })
                .then(function (response) {
                    alert("successfully done");
                    $state.go('transactions');
                }, function (err) { console.log(err); });
            };
            $ctrl.getReceiverSuggestions = function () {
                TransactionsService.getReceiverSuggestions($ctrl.search).then(function (response) {
                    $ctrl.receiverSuggestions = response.data;
                }, function (err) { $ctrl.receiverSuggestions = []; });
            }
        }]
})