﻿angular.module('pw')
.component('register', {
    templateUrl: "templates/register.html",
    controller: ['$rootScope', 'AuthService', '$state', function ($rootScope, AuthService, $state) {
        if ($rootScope.isLoggedIn) {
            $state.go('main');
        }
        var $ctrl = this;
        $ctrl.formData = {
            EMail: "",
            FullName: "",
            Password: "",
            ConfirmPassword: ""
        };
        $ctrl.submit = function () {
            AuthService.register($ctrl.formData).then(function (response) {
                alert("Successfully registered!");
                $state.go('login');
            },
            function (err) {
                alert(err.data.Message);
            })
        };
    }]
})