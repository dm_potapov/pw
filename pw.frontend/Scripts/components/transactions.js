﻿angular.module('pw')
.component('transactions', {
    templateUrl: "templates/transactions_history.html",
    controller: ['TransactionsService', function (TransactionsService) {
        var $ctrl = this;
        if (!$rootScope.isLoggedIn) {
            $state.go('login');
        }
        $ctrl.transactions = [];
        $ctrl.filter = {
            From: "",
            To: "",
            Name: ""
        }
        $ctrl.getTransactions = function () {
            TransactionsService.getHistory($ctrl.filter).then(function (response) {
                $ctrl.transactions = response.data;
            });
        }
        $ctrl.getTransactions();
    }]
})