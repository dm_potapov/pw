﻿using pw.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace pw.Storage
{
    public class EFRepository<T> : IGuidRepository<T> where T : class, IEntity<Guid>
    {
        private readonly DbContext _db;
        private readonly IDbSet<T> _dbSet;

        public EFRepository(DbContext db)
        {
            if (db == null) throw new ArgumentNullException(nameof(db));
            _db = db;
            _dbSet = db.Set<T>();
        }

        public Type ElementType
        {
            get
            {
                return _dbSet.ElementType;
            }
        }

        public Expression Expression
        {
            get
            {
                return _dbSet.Expression;
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                return _dbSet.Provider;
            }
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbSet.SingleOrDefaultAsync(obj => obj.Id == id);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _dbSet.GetEnumerator();
        }

        public async Task AddAsync(T item)
        {
            var existingItem = await GetByIdAsync(item.Id);
            if (existingItem == null)
            {
                _dbSet.Add(item);
                await _db.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("element with specified id already exists");
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dbSet.GetEnumerator();
        }

        public async Task SaveChangesAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}