﻿using pw.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace pw.Storage
{
    public interface IRepository<T, TKey> : IQueryable<T>
    {
        Task<T> GetByIdAsync(TKey id);

        Task AddAsync(T item);

        Task SaveChangesAsync();
    }
}