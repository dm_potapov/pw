﻿using pw.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace pw.Storage
{
    public class PwDbContext : DbContext
    {
        public PwDbContext() : base("pwAuth")
        { }

        protected virtual IDbSet<User> Users { get; set; }

        protected virtual IDbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<User>().Property(u => u.EMail)
                                  .IsRequired()
                                  .HasMaxLength(128)
                                  .HasColumnAnnotation("Index",
                                                       new IndexAnnotation(new IndexAttribute("IX_Name") { IsUnique = true }));
            builder.Entity<User>().Property(u => u.PasswordHash).IsRequired();
            builder.Entity<Transaction>().HasRequired(t => t.Sender)
                                         .WithMany(u => u.GivenTransactions)
                                         .WillCascadeOnDelete(false);
            builder.Entity<Transaction>().HasRequired(t => t.Receiver)
                                         .WithMany(u => u.ReceivedTransactions)
                                         .WillCascadeOnDelete(false);
        }
    }
}