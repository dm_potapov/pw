﻿using pw.BusinessLogic.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pw.Storage
{
    public class UsersDataStorage
    {
        private static Dictionary<Guid, UserAuthData> _dict;

        static UsersDataStorage()
        {
            _dict = new Dictionary<Guid, UserAuthData>();
        }

        public static Guid Put(UserAuthData data)
        {
            var newSessionId = Guid.NewGuid();
            _dict.Add(newSessionId, data);
            return newSessionId;
        }

        public static UserAuthData Get(Guid sessionId)
        {
            if (_dict.ContainsKey(sessionId))
            {
                var data = _dict[sessionId];
                if (data.SessionValidUntil < DateTime.UtcNow)
                {
                    RemoveData(sessionId);
                }
                else
                {
                    return data;
                }
            }
            return null;
        }

        public static void RemoveData(Guid sessionId)
        {
            _dict.Remove(sessionId);
        }
    }
}