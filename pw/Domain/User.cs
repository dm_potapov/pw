﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pw.Models.Auth;

namespace pw.Domain
{
    public class User : IEntity<Guid>
    {
        private UserRegisterModel userModel;

        public User() { }

        public User(string email, string fullname, string passwordHash, decimal amount = 500)
        {
            if (string.IsNullOrEmpty(email)) throw new ArgumentNullException(nameof(email));
            if (string.IsNullOrEmpty(fullname)) throw new ArgumentNullException(nameof(fullname));
            if (string.IsNullOrEmpty(passwordHash)) throw new ArgumentNullException(nameof(passwordHash));

            Id = Guid.NewGuid();
            EMail = email;
            FullName = fullname;
            PasswordHash = passwordHash;
            Amount = amount;
        }

        public Guid Id { get; set; }

        public string EMail { get; set; }

        public string FullName { get; set; }

        public string PasswordHash { get; set; }

        public decimal Amount { get; set; }

        public virtual ICollection<Transaction> GivenTransactions { get; set; }

        public virtual ICollection<Transaction> ReceivedTransactions { get; set; }
    }
}