﻿using System;

namespace pw.Domain
{
    public class Transaction : IEntity<Guid>
    {
        public Transaction()
        {
            Id = Guid.NewGuid();
        }

        public Transaction(User sender, User receiver, DateTime date, decimal amount) : this()
        {
            if (sender == null) throw new ArgumentNullException(nameof(sender));
            if (receiver == null) throw new ArgumentNullException(nameof(receiver));

            Sender = sender;
            Receiver = receiver;
            Date = date;
            Amount = amount;
        }

        public Guid Id { get; set; }

        public virtual User Sender { get; set; }

        public virtual User Receiver { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public decimal SenderResidue { get; set; }

        public decimal ReceiverResidue { get; set; }
    }
}