﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace System.Collections.Generic
{
    public static class DictionaryExtensions
    {
        public static void AddOrChangeValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
            else
            {
                dictionary[key] = value;
            }
        }

        public static ModelError FirstModelError(this ModelStateDictionary dict)
        {
            foreach (var value in dict.Values)
            {
                if (value.Errors != null && value.Errors.Any())
                {
                    return value.Errors.First();
                }
            }

            return null;
        }
    }
}