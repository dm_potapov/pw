﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace pw.Helpers
{
    public class SHAHasher : IHasher
    {
        private static SHA256Managed _cryptoProvider = new SHA256Managed();

        public string Hash(string data)
        {
            var bytes = Encoding.UTF8.GetBytes(data);
            var hashedBytes = _cryptoProvider.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hashedBytes)
            {
                hashString += string.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}