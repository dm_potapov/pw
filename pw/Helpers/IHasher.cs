﻿namespace pw.Helpers
{
    public interface IHasher
    {
        string Hash(string data);
    }
}