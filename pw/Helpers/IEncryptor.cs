﻿namespace pw.Helpers
{
    public interface IEncryptor
    {
        byte[] Encrypt(byte[] bytes);

        byte[] Decrypt(byte[] bytes);
    }
}