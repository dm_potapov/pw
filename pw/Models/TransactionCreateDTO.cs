﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pw.Models
{
    public class TransactionCreateDTO
    {
        [Required]
        public Guid ReceiverId { get; set; }

        [Required]
        public decimal Amount { get; set; }
    }
}