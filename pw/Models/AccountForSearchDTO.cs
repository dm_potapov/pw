﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pw.Models
{
    public class AccountForSearchDTO
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
    }
}