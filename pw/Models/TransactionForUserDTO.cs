﻿using pw.Domain;
using System;

namespace pw.Models
{
    public class TransactionForUserDTO
    {
        public TransactionForUserDTO(Transaction t, User u)
        {
            var isUserRoleSender = (t.Sender.Id == u.Id);
            var SecondParticipant = isUserRoleSender ? t.Sender : t.Receiver;
            SecondParticipantId = SecondParticipant.Id;
            SecondParticipantName = SecondParticipant.FullName;
            Date = t.Date;
            Amount = isUserRoleSender ? -t.Amount : t.Amount;
            Residue = isUserRoleSender ? t.SenderResidue : t.ReceiverResidue;
        }

        public Guid SecondParticipantId { get; set; }

        public string SecondParticipantName { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public decimal Residue { get; set; }
    }
}