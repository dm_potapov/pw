﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pw.Models.Auth
{
    public class UserRegisterModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "EMail address is incorrect")]
        public string EMail { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "Password must be at least 8 characters long")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Passwords don't match")]
        public string ConfirmPassword { get; set; }
    }
}