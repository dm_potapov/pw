﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pw.Models.Auth
{
    public class UserLoginModel
    {
        [Required]
        public string EMail { get; set; }

        [Required]
        public string Password { get; set; }
    }
}