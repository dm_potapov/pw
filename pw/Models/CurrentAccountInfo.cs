﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pw.Models
{
    public class CurrentAccountInfo
    {
        public string FullName { get; set; }

        public decimal Amount { get; set; }
    }
}