﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pw.Models
{
    public class TransactionsHistoryFilterDTO
    {
        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public string Name { get; set; }
    }
}