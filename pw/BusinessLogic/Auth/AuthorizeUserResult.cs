﻿using pw.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pw.BusinessLogic.Auth
{
    public class AuthorizeUserResult
    {
        public bool IsSucceeded { get; set; }

        public string Message { get; set; }

        public string Token { get; set; }

        public CurrentAccountInfo AccountInfo { get; set; }
    }
}