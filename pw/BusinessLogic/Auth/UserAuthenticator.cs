﻿using pw.Domain;
using pw.Helpers;
using pw.Models;
using pw.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace pw.BusinessLogic.Auth
{
    public class UserAuthenticator
    {
        IGuidRepository<User> _users;
        TokenManager _tokenManager;
        IHasher _hasher;

        public UserAuthenticator(IGuidRepository<User> users, TokenManager tokenManager, IHasher hasher)
        {
            if (users == null) throw new ArgumentNullException(nameof(users));
            if (tokenManager == null) throw new ArgumentNullException(nameof(tokenManager));
            if (hasher == null) throw new ArgumentNullException(nameof(hasher));

            _users = users;
            _tokenManager = tokenManager;
            _hasher = hasher;
        }

        public async Task<AuthorizeUserResult> Authenticate(string email, string password)
        {
            var res = new AuthorizeUserResult()
            {
                IsSucceeded = false
            };
            var user = _users.SingleOrDefault(u => u.EMail == email);
            if (user == null || user.PasswordHash != _hasher.Hash(password))
            {
                res.Message = "Specified login or password is not correct";
            }
            else
            {
                var sessionId = UsersDataStorage.Put(new UserAuthData()
                {
                    Login = user.EMail,
                    SessionValidUntil = DateTime.UtcNow.AddDays(1)
                });
                res.Token = _tokenManager.GetToken(sessionId);
                res.AccountInfo = new CurrentAccountInfo
                {
                    Amount = user.Amount,
                    FullName = user.FullName
                };
                res.IsSucceeded = true;
            }

            return res;
        }
    }
}