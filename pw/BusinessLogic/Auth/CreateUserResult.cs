﻿namespace pw.BusinessLogic.Auth
{
    public class CreateUserResult
    {
        public bool IsSucceeded { get; set; }

        public string Message { get; set; }
    }
}