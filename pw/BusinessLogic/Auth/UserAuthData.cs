﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pw.BusinessLogic.Auth
{
    public class UserAuthData
    {
        public string Login { get; set; }

        public DateTime SessionValidUntil { get; set; }
    }
}