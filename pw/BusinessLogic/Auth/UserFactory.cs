﻿using pw.Domain;
using pw.Helpers;
using pw.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace pw.BusinessLogic.Auth
{
    public class UserFactory
    {
        private readonly IGuidRepository<User> _users;
        private readonly IHasher _hasher;

        public UserFactory(IGuidRepository<User> users, IHasher hasher)
        {
            if (users == null) throw new ArgumentNullException(nameof(users));
            if (hasher == null) throw new ArgumentNullException(nameof(hasher));

            _users = users;
            _hasher = hasher;
        }

        public async Task<CreateUserResult> CreateUser(string eMail, string fullName, string password)
        {
            if (string.IsNullOrEmpty(eMail)) throw new ArgumentNullException(nameof(eMail));
            if (string.IsNullOrEmpty(fullName)) throw new ArgumentNullException(nameof(fullName));
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException(nameof(password));

            var res = new CreateUserResult()
            {
                IsSucceeded = false
            };

            if (_users.Any(u => u.EMail == eMail))
            {
                res.Message = "User with specified Email already exists";
            }
            else
            {
                var user = new User(eMail, fullName, _hasher.Hash(password));
                await _users.AddAsync(user);
                res.IsSucceeded = true;
            }

            return res;
        }
    }
}