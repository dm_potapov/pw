﻿using pw.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace pw.BusinessLogic.Auth
{
    public class TokenManager
    {
        private readonly IEncryptor _encryptor;

        public TokenManager(IEncryptor encryptor)
        {
            if (encryptor == null) throw new ArgumentNullException(nameof(encryptor));

            _encryptor = encryptor;
        }

        public string GetToken(Guid sessionId)
        {
            return Convert.ToBase64String(_encryptor.Encrypt(sessionId.ToByteArray()));
        }

        public Guid GetSessionIdByToken(string token)
        {
            return new Guid(_encryptor.Decrypt(Convert.FromBase64String(token)));
        }
    }
}