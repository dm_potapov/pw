﻿using pw.BusinessLogic.Auth;
using pw.Models.Auth;
using pw.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace pw.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        private readonly UserFactory _factory;
        private readonly UserAuthenticator _authenticator;

        public LoginController(UserFactory factory, UserAuthenticator authenticator)
        {
            if (factory == null) throw new ArgumentNullException(nameof(factory));
            if (authenticator == null) throw new ArgumentNullException(nameof(authenticator));
            
            _factory = factory;
            _authenticator = authenticator;
        }

        [Route("signup")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> SignUp(UserRegisterModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.FirstModelError().ErrorMessage);
            }

            var createResult = await _factory.CreateUser(userModel.EMail, userModel.FullName, userModel.Password);

            if (!createResult.IsSucceeded)
            {
                return BadRequest(createResult.Message);
            }

            return Ok();
        }

        [Route("signin")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> SignIn(UserLoginModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.FirstModelError().ErrorMessage);
            }

            var authResult = await _authenticator.Authenticate(userModel.EMail, userModel.Password);

            if (!authResult.IsSucceeded)
            {
                return BadRequest(authResult.Message);
            }

            return Ok(new
            {
                Token = authResult.Token,
                AccountInfo = authResult.AccountInfo
            });
        }

        [HttpGet]
        [Route("logout")]
        public IHttpActionResult LogOut()
        {
            var currentSessionId = (Guid)Request.Properties["sessionId"];
            UsersDataStorage.RemoveData(currentSessionId);
            return Ok();
        }
    }
}
