﻿using pw.Domain;
using pw.Models;
using pw.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace pw.Controllers
{
    [RoutePrefix("api/transactions")]
    public class TransactionsController : ApiController
    {
        IGuidRepository<Transaction> _transactions;
        IGuidRepository<User> _users;

        public TransactionsController(IGuidRepository<Transaction> transactions, 
                                      IGuidRepository<User> users)
        {
            if (transactions == null) throw new ArgumentNullException(nameof(transactions));
            if (users == null) throw new ArgumentNullException(nameof(users));

            _transactions = transactions;
            _users = users;
        }

        [HttpPost]
        [Route("commit")]
        public async Task<IHttpActionResult> CommitTransaction(TransactionCreateDTO transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var currentEMail = (string)Request.Properties["currentEMail"];
            var sender = _users.SingleOrDefault(u => u.EMail == currentEMail);

            if (sender.Amount < transaction.Amount)
            {
                return BadRequest("Transaction amount is more than account sum");
            }
            else
            {
                var receiver = await _users.GetByIdAsync(transaction.ReceiverId);
                receiver.Amount += transaction.Amount;
                sender.Amount -= transaction.Amount;
                await _users.SaveChangesAsync();

                await _transactions.AddAsync(new Transaction(sender, receiver, DateTime.UtcNow, transaction.Amount));

                return Ok();
            }
        }

        [HttpPost]
        [Route("history")]
        public async Task<IHttpActionResult> GetTransactionsHistoryForUser(TransactionsHistoryFilterDTO filter)
        {
            var currentEMail = (string)Request.Properties["currentEMail"];
            var user = _users.SingleOrDefault(u => u.EMail == currentEMail);
            var transactionsQuery = _transactions.Where(t => t.Receiver.EMail == currentEMail
                                                           || t.Sender.EMail == currentEMail);
            if (filter != null)
            {
                if (filter.From.HasValue)
                {
                    transactionsQuery = transactionsQuery.Where(t => t.Date >= filter.From.Value);
                }
                if (filter.To.HasValue)
                {
                    transactionsQuery = transactionsQuery.Where(t => t.Date <= filter.To.Value);
                }
                if (!string.IsNullOrEmpty(filter.Name))
                {
                    transactionsQuery = transactionsQuery.Where(t => t.Receiver.FullName == filter.Name
                                                                    || t.Sender.FullName == filter.Name);
                }
            }

            return Ok(transactionsQuery.ToList().Select(t => new TransactionForUserDTO(t, user)));
        }
    }
}
