﻿namespace pw.Controllers
{
    using Attributes;
    using Domain;
    using Models;
    using Storage;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web.Http;

    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private readonly IGuidRepository<User> _users;

        public AccountController(IGuidRepository<User> users)
        {
            if (users == null) throw new ArgumentNullException(nameof(users));

            _users = users;
        }

        [Route("info")]
        public async Task<CurrentAccountInfo> GetCurrentUserAccountInfo()
        {
            var currentEMail = (string)Request.Properties["currentEMail"];
            var currentAccount = _users.SingleOrDefault(u => u.EMail == currentEMail);

            return new CurrentAccountInfo
            {
                FullName = currentAccount.FullName,
                Amount = currentAccount.Amount
            };
        }

        [HttpGet]
        [Route("GetByFullName")]
        public async Task<IHttpActionResult> GetAccountsByFullName(string search)
        {
            var currentEMail = (string)Request.Properties["currentEMail"];
            var users = _users.ToList().Where(u => u.EMail != currentEMail && 
                                                 u.FullName.IndexOf(search, StringComparison.InvariantCultureIgnoreCase) != -1).Take(5);
            if (!users.Any())
            {
                return NotFound();
            }
            else
            {
                return Ok(users.ToList().Select(user => new AccountForSearchDTO()
                {
                    FullName = user.FullName,
                    Id = user.Id
                }));
            }
        }
    }
}
