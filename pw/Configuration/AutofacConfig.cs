﻿using Autofac;
using Autofac.Integration.WebApi;
using pw.BusinessLogic.Auth;
using pw.Helpers;
using pw.Storage;
using System.Data.Entity;
using System.Reflection;
using System.Web.Http;

namespace pw.Configuration
{
    public class AutofacConfig
    {
        public static IContainer GetContainer(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance<DbContext>(new PwDbContext());
            builder.RegisterGeneric(typeof(EFRepository<>)).As(typeof(IGuidRepository<>)).InstancePerRequest();

            builder.RegisterType<TokenManager>();
            builder.RegisterType<UserAuthenticator>();
            builder.RegisterType<UserFactory>();

            builder.RegisterInstance<IHasher>(new SHAHasher());
            builder.RegisterInstance<IEncryptor>(new SimpleAESEncryptor());

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);
            return builder.Build();
        }
    }
}