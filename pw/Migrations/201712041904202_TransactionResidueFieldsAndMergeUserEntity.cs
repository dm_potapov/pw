namespace pw.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransactionResidueFieldsAndMergeUserEntity : DbMigration
    {
        public override void Up()
        {
            Sql("alter table dbo.UserAccounts drop constraint [FK_dbo.UserAccounts_dbo.Users_Id]");
            Sql("alter table dbo.Transactions drop constraint [FK_dbo.Transactions_dbo.UserAccounts_Receiver_Id]");
            Sql("alter table dbo.Transactions drop constraint [FK_dbo.Transactions_dbo.UserAccounts_Sender_Id]");
            AddForeignKey("dbo.Transactions", "Sender_Id", "dbo.Users");
            AddForeignKey("dbo.Transactions", "Receiver_Id", "dbo.Users");
            DropIndex("dbo.UserAccounts", new[] { "Id" });
            AddColumn("dbo.Transactions", "SenderResidue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Transactions", "ReceiverResidue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Users", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            Sql("update dbo.Users set Amount = (select Amount from dbo.UserAccounts where Id = dbo.Users.Id)");
            AlterColumn("dbo.Users", "EMail", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Users", "PasswordHash", c => c.String(nullable: false));
            CreateIndex("dbo.Users", "EMail", unique: true, name: "IX_Name");
            DropTable("dbo.UserAccounts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserAccounts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            Sql("insert into dbo.UserAccounts select id, Amount from dbo.Users");
            CreateIndex("dbo.UserAccounts", "Id");
            AddForeignKey("dbo.UserAccounts", "Id", "dbo.Users", "Id");
            DropIndex("dbo.Users", "IX_Name");
            AlterColumn("dbo.Users", "PasswordHash", c => c.String());
            AlterColumn("dbo.Users", "EMail", c => c.String());
            Sql("update dbo.UserAccounts set Amount = (select Amount from dbo.Users where Id = dbo.UserAccounts.Id)");
            DropColumn("dbo.Users", "Amount");
            DropColumn("dbo.Transactions", "ReceiverResidue");
            DropColumn("dbo.Transactions", "SenderResidue");
            Sql("alter table dbo.Transactions drop constraint [FK_dbo.Transactions_dbo.Users_Receiver_Id]");
            Sql("alter table dbo.Transactions drop constraint [FK_dbo.Transactions_dbo.Users_Sender_Id]");
            AddForeignKey("dbo.Transactions", "Sender_Id", "dbo.UserAccounts", "Id");
            AddForeignKey("dbo.Transactions", "Receiver_Id", "dbo.UserAccounts", "Id");
        }
    }
}
