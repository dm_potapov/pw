﻿using pw.BusinessLogic.Auth;
using pw.Storage;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace pw.Attributes
{
    public class AuthorizeByTokenAttribute : AuthorizeAttribute
    {
        private const string _currentEMail = "currentEMail";
        private const string _sessionId = "sessionId";

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var token = actionContext.Request.Headers.Authorization?.Parameter;

            if (token != null)
            {
                var tokenManager = actionContext.ControllerContext.Configuration
                                                .DependencyResolver.GetService(typeof(TokenManager)) as TokenManager;
                try
                {
                    var sessionId = tokenManager.GetSessionIdByToken(token);

                    var userData = UsersDataStorage.Get(sessionId);

                    if (userData != null)
                    {
                        actionContext.Request.Properties.AddOrChangeValue(_currentEMail, userData.Login);
                        actionContext.Request.Properties.AddOrChangeValue(_sessionId, sessionId);
                        return true;
                    }
                }
                catch(Exception) { }
            }

            return false;
        }
    }
}